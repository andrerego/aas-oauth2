package com.example.aas;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;

/**
 * Created by ANDRE REGO on 22/07/2019.
 */

@RestController
@RequestMapping(value = "/conta")
public class ContaController {

    @Autowired
    private ContaRepository repo;

    @Value( "${spring.security.oauth2.client.clientId}" )
    private String clientId;

    @Value( "${spring.security.oauth2.client.clientSecret}" )
    private String clientSecret;

    //Definimos o restTemplate
    private RestTemplate restTemplate;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Optional<Conta>> find(@PathVariable Integer id) {

        Optional<Conta> obj = repo.findById(id);
        System.out.println(obj);
        if(obj.isPresent()){
            return ResponseEntity.ok().body(obj);
        }
        String url = "https://www.facebook.com/v3.3/dialog/oauth?client_id="+clientId+"&redirect_uri=http://localhost:8080/conta/cadastro/"+id+"?code={code}&state=acd";
        HttpHeaders headers = new HttpHeaders();
        headers.add("Location", url);
        return new ResponseEntity<>(headers, HttpStatus.TEMPORARY_REDIRECT);

    }


    @RequestMapping(value = "/cadastro/{id}", method = RequestMethod.GET)
    public ResponseEntity<Conta> cadastro(@PathVariable String id, @RequestParam String code, @RequestParam String state) {
        String tokenURL = "https://graph.facebook.com/v3.3/oauth/access_token?client_id="+clientId+
                "&redirect_uri=https://localhost:8080/&state="+state+"&client_secret="+clientSecret+"&code="+code;
        System.out.println("Error "+ tokenURL);
        ResponseEntity<Conta> responseEntity = restTemplate.exchange(tokenURL, HttpMethod.GET, null, Conta.class);
        System.out.println("Error"+ code + responseEntity);
        return new ResponseEntity<Conta>(HttpStatus. OK);
    }

}
