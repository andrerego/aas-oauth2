package com.example.aas;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by ANDRE REGO on 22/07/2019.
 */
@Data
@Entity
public class Conta {
    @Id
    private Integer id;
    private String nome;
    private Integer idFacebook;
}
